# Star Wars Planets API

# Description 

Made for professional purposes.

# Routes 

- Port: :8080
- /searchPlanets ["GET"]
- /searchPlanetByName/{name} ["GET"]
- /searchPlanetById/{id} ["GET"]
- /addPlanet ["POST"] ([Body: x-www-form-urlencoded], Params:[name: string, weather: string, terrain: string])
- /deletePlanet ["POST"] ([Body: x-www-form-urlencoded], Params:[id: string])

# Documentation used

- https://swapi.dev/documentation
- https://github.com/peterhellberg/swapi
- https://github.com/gin-gonic/gin
- https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

# Things I would do if I had more time (the famous "TO DO")

- Add JWT auth (https://github.com/dgrijalva/jwt-go)
- Add .env file to control staging and production environments (https://github.com/joho/godotenv)
- Implement paid Mongo DB Atlas for better performance (the free version presents instability)
- TDD (with testing package)
- Try to improve indentation of some methods

# How to test

- 1: Please, install Mongo DB locally (I didn't use the Atlas Cloud version because was too unstable)
- 2: start Mongo DB instance
- 3: git clone the GitLab project
- 4: import the earthTest.json file with the command:

[project root] sudo mongoimport --db Star-Wars-API --collection planets --file earthTest.json

- 5: now that you have created the database, run the application with the command:

(Linux/MacOS) [project root] sudo ./star-wars-planets-api
(Windows) double click on star-wars-planets-api.exe file (need to verify this, by the way)

 and start testing the routes with Postman or similar (above on #Routes session)


 Obs 1: depending on the platform you're testing, you'll need Golang installed (I think this is for Windows, need to confirm)

 Obs 2: try adding only existing planets, like Tatooine, Alderaan, Hoth or Kamino, because the system let's you write any planets, like Earth e.g., but it will search all the SWAPI for the planet before saving it on database, and that takes a little time (1-2 minutes)

 Hope you enjoy! =)
