package messages

const PLANETNOTFOUNDWITHNAME = "No planet found with this name, try adding it with /addPlanet POST endpoint."
const PLANETNOTFOUNDWITHID = "No planet found with this ID, try adding it with /addPlanet POST endpoint."
const NOPLANETPASSED = "It appears you haven't inserted a planet to search, try setting it's name after the endpoint with '/planetName' "
const NOIDPASSED = "It appears you haven't inserted an id to search, try setting after the endpoint with '/planetId' "
const PLANETINSERTED = "Planet inserted successfully!"
const ALLFIELDSREQUIRED = "All fields (name, weather and terrain) are required."
const PLANETDELETED = "Planet deleted successfully!"
const IDNOTGIVEN = "No ID has been given, please provide one."
const PLANETDOESNTEXIST = "It doesn't exist a planet with this ID to delete"