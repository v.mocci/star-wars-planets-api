package routing

import (
	"github.com/gin-gonic/gin"
	getPlanets "gitlab.com/v.mocci/star-wars-planets-api/app/handlers/getPlanets"
	addPlanet "gitlab.com/v.mocci/star-wars-planets-api/app/handlers/addPlanet"
	deletePlanet "gitlab.com/v.mocci/star-wars-planets-api/app/handlers/deletePlanet"
)

func Routes(){
	router := gin.Default()
	router.GET("/searchPlanets", getPlanets.ReturnPlanets)
	router.GET("/searchPlanetByName/:name", getPlanets.ReturnPlanetByName)	
	router.GET("/searchPlanetById/:id", getPlanets.ReturnPlanetById)
	router.GET("/searchPlanetById", getPlanets.ReturnErrorMessageId)
	router.GET("/searchPlanetByName", getPlanets.ReturnErrorMessageName)
	router.POST("/addPlanet", addPlanet.AddPlanet)
	router.POST("/deletePlanet", deletePlanet.DeletePlanet)
	router.Run()
}