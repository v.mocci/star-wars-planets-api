package getPlanets

import (
	connection "gitlab.com/v.mocci/star-wars-planets-api/config/database"
	"go.mongodb.org/mongo-driver/bson"
	"github.com/gin-gonic/gin"
	"context"
	"log"
	"time"
	"fmt"
)

func ReturnPlanets(contxt *gin.Context){
	
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)	
	planetsCollection := connection.Connection().Database(connection.DATABASE).Collection(connection.PLANETSCOLLECTION)
	
	filter, err := planetsCollection.Find(ctx, bson.M{})

		if err != nil {
		    log.Fatal(err)
		}

	var planetsFiltered []bson.M

		if err = filter.All(ctx, &planetsFiltered); err != nil {
		    log.Fatal(err)
		}

		fmt.Println(planetsFiltered)
	
	contxt.JSON(200, gin.H{		
		"planets": planetsFiltered,		
	})
}
