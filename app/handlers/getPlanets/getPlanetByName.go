package getPlanets

import (	
	connection "gitlab.com/v.mocci/star-wars-planets-api/config/database"
	MESSAGES "gitlab.com/v.mocci/star-wars-planets-api/config/messages"	
	"go.mongodb.org/mongo-driver/bson"	
	"github.com/gin-gonic/gin"
	"context"
	"log"
	"time"		
)

func ReturnPlanetByName(contxt *gin.Context){
	planetNameinput := contxt.Param(NAME)

		if (verifyIfPlanetIsNull(planetNameinput)){
			contxt.JSON(404, gin.H{})
			return
		}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)	
	planetsCollection := connection.Connection().Database(connection.DATABASE).Collection(connection.PLANETSCOLLECTION)
	
	filter, err := planetsCollection.Find(ctx, bson.M{NAME: planetNameinput})

		if err != nil {
		    log.Fatal(err)
		}
			
	var planetFiltered []bson.M

		if err = filter.All(ctx, &planetFiltered); err != nil {
		    log.Fatal(err)
		}

		if(verifyIfResultIsNul(planetFiltered)){
			contxt.JSON(200, gin.H{		
				"message": MESSAGES.PLANETNOTFOUNDWITHNAME,				
			})
			return
		}
	
	contxt.JSON(200, gin.H{		
		"planet_id": planetFiltered[0]["_id"],
		"planet_name": planetFiltered[0]["name"],
		"planet_weather": planetFiltered[0]["weather"],
		"planet_terrain": planetFiltered[0]["terrain"],
		"planet_appearances": planetFiltered[0]["appearances"],
	})
}
