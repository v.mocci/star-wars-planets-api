package getPlanets

import (
	MESSAGES "gitlab.com/v.mocci/star-wars-planets-api/config/messages"
	"go.mongodb.org/mongo-driver/bson"	
	"github.com/gin-gonic/gin"
)

const ISNULL string = ""
const NAME string = "name" 
const ID string = "id"
const PLANETID string = "_id"

func verifyIfPlanetIsNull(planetName string) bool{
	if (planetName == ISNULL){
		return true
	}
	return false
}

func verifyIfResultIsNul(result []bson.M) bool{
	if (result == nil){
		return true
	}
	return false
}

func ReturnErrorMessageName(contxt *gin.Context){	
	contxt.JSON(200, gin.H{		
		"message": MESSAGES.NOPLANETPASSED,
	})	 
}

func ReturnErrorMessageId(contxt *gin.Context){	
	contxt.JSON(200, gin.H{		
		"message": MESSAGES.NOIDPASSED,
	})	 
}