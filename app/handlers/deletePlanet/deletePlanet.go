package deletePlanet

import (	
	MESSAGES "gitlab.com/v.mocci/star-wars-planets-api/config/messages"
	connection "gitlab.com/v.mocci/star-wars-planets-api/config/database"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/bson"
	"github.com/gin-gonic/gin"
	"time"
	"context"
	"log"
	"fmt"
)

const ID = "id"
const EMPTY = ""

func DeletePlanet(contxt *gin.Context){
	planetIdInput := contxt.PostForm(ID)

	if (verifyIfIdToDeleteIsEmpty(planetIdInput)){
		contxt.JSON(200, gin.H{
			"message": MESSAGES.IDNOTGIVEN,
		})
		return
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)	
	planetsCollection := connection.Connection().Database(connection.DATABASE).Collection(connection.PLANETSCOLLECTION)
	planetIdInputHex, err := primitive.ObjectIDFromHex(planetIdInput)

	verifyIfIdExists, err := planetsCollection.Find(ctx, bson.M{"_id": planetIdInputHex})

	var verificationFiltered []bson.M 

	if err = verifyIfIdExists.All(ctx, &verificationFiltered); err != nil {
		log.Fatal(err)
	}

	if (verifyIfIDExistsToDelete(verificationFiltered)){
		contxt.JSON(200, gin.H{
			"message": MESSAGES.PLANETDOESNTEXIST,
		})
		return
	}

	result, err := planetsCollection.DeleteOne(ctx, bson.M{"_id": planetIdInputHex})
		
		if err != nil {
		    log.Fatal(err)
		}

		fmt.Println("Planet deleted", result.DeletedCount)

	contxt.JSON(200, gin.H{
		"message": MESSAGES.PLANETDELETED,
	})	 
}

func verifyIfIdToDeleteIsEmpty(idToDelete string) bool {
	if(idToDelete == EMPTY){
		return true
	}
	return false
}

func verifyIfIDExistsToDelete(verificationFiltered []bson.M ) bool {
	if (verificationFiltered == nil){
		return true
	}
	return false
}