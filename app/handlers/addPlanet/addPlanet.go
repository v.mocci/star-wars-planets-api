package addPlanet

import (
	"github.com/gin-gonic/gin"
	MESSAGES "gitlab.com/v.mocci/star-wars-planets-api/config/messages"
	connection "gitlab.com/v.mocci/star-wars-planets-api/config/database"
	"go.mongodb.org/mongo-driver/bson"
	"github.com/peterhellberg/swapi"
	"time"
	"context"
	"log"
	"fmt"
)

const EMPTY = ""
const TOTALSWPLANETS = 60

type Planet struct {
	name string
	weather string
	terrain string
	numberAppearances int
}

func AddPlanet(contxt *gin.Context){

	var planet Planet
	
	planet.name = contxt.PostForm("name")
	planet.weather = contxt.PostForm("weather")
	planet.terrain = contxt.PostForm("terrain")
	planet.numberAppearances = 0

	if(verifyIfInputsAreEmpty(planet)){
		contxt.JSON(200, gin.H{
			"message": MESSAGES.ALLFIELDSREQUIRED,			
		})	
		return 
	}

	swapiClient := swapi.DefaultClient

	for planetId := 1; planetId < TOTALSWPLANETS; {
		if atst, err := swapiClient.Planet(planetId); err == nil {
			if(atst.Name == planet.name){
				planet.numberAppearances = len(atst.FilmURLs)
				planetId = TOTALSWPLANETS				
			}					
		}
		planetId++ 
	}
		
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)	
	planetsCollection := connection.Connection().Database(connection.DATABASE).Collection(connection.PLANETSCOLLECTION)
	
	planetResult, err := planetsCollection.InsertOne(ctx, bson.D{
		{Key: "name", Value: planet.name},
		{Key: "weather", Value: planet.weather},
		{Key: "terrain", Value: planet.terrain},
		{Key: "appearances", Value: planet.numberAppearances},
	})

		if err != nil {
				log.Fatal(err)
		}

		fmt.Println(planetResult.InsertedID)
	
	contxt.JSON(200, gin.H{
		"message": MESSAGES.PLANETINSERTED,
		"planet": planet.name,
		"weather": planet.weather, 
		"terrain": planet.terrain,
		"appearances": planet.numberAppearances,
	})	 
}

func verifyIfInputsAreEmpty(input Planet) bool {
	if(input.name == EMPTY || input.weather == EMPTY || input.terrain == EMPTY){
		return true
	}
	return false
}